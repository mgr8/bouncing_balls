import turtle, time, random

wn = turtle.Screen()
wn.bgcolor("black")
wn.title("Bouncing Ball Simulator")
wn.tracer(0)

balls = []
colors = ['#fe9f97', '#00aabb', '#de3d83', '#3dbd5d']
shapes = ['arrow', 'turtle', 'square', 'circle', 'triangle', 'classic']
for _ in range(25):
	balls.append(turtle.Turtle())


for ball in balls:
	ball.shape(random.choice(shapes))
	ball.color(random.choice(colors))
	ball.penup()
	ball.speed(0) # this is the animation speed, not the ball speed
	ball.goto(random.randint(-290, 290), random.randint(190, 290))
	ball.dy = 0 # ball's velocity in y dir
	ball.dx = random.randint(-3, 3) # ball's velocity in x dir
	ball.da = random.randint(-3, 3) # ball's rotational velocity 

gravity = 0.1

while True:
	wn.update()
	for ball in balls:
		time.sleep(0.0003)
		ball.rt(ball.da)
		ball.dy -= gravity
		ball.sety(ball.ycor() + ball.dy)

		ball.setx(ball.xcor() + ball.dx)

		# Check for wall collison
		if ball.xcor() > 300 or ball.xcor() < -300:
			ball.dx *= -1
			ball.da *= -1

		# Check for bounce
		if ball.ycor() < -300:
			ball.sety(-300)
			ball.dy *= -1
			ball.da *= -1

wn.mainloop()